name := "ci-helloworld"

version := "0.1-SNAPSHOT"

scalaVersion := "2.12.5"

lazy val akkaVersion = "2.5.16"

libraryDependencies ++= Seq(
  "com.typesafe.play" %% "play-json" % "2.6.10",
  "com.typesafe.akka" %% "akka-stream" % akkaVersion,
  "org.scalatest" %% "scalatest" % "3.0.5" % "test"
)
