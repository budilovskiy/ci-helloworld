import play.api.libs.json._

import scala.util.Try

object Main extends App {

  val json: JsValue = Json.parse("""
  {
    "name" : "Watership Down",
    "location" : {
      "lat" : 51.235685,
      "long" : -1.309197
    },
    "residents" : [ {
      "name" : "Fiver",
      "age" : 4,
      "role" : null
    }, {
      "name" : "Bigwig",
      "age" : 6,
      "role" : "Owsla"
    } ]
  }
  """)

  println(json)

  val json2 = Try(Json.parse("Refund - Cancel Order")).toOption.getOrElse("Refund - Cancel Order")


  println(json2)
}
