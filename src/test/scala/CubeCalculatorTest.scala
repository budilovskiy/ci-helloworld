import org.scalatest.FunSuite

class CubeCalculatorTest extends FunSuite {
  test("CubeCalculator.cube3") {
    assert(CubeCalculator.cube(3) === 27)
  }
  test("CubeCalculator.cube1") {
    assert(CubeCalculator.cube(1) === 1)
  }
  test("CubeCalculator.cube2") {
    assert(CubeCalculator.cube(2) === 8)
  }
}